export interface ModalObject {
  open: boolean;
  data: Array;
}
