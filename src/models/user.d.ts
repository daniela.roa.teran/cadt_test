import { BaseObject } from ".";

export interface User extends BaseObject {
  id: number;
  name: string;
}
export type UserList = User[];
