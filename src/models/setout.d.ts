export interface Setout {
  id: number;
  updated: Date;
  name: string;
  machine_name: string;
  machine_width: number;
  courses: number;
}
export type SetoutList = Setout[];
