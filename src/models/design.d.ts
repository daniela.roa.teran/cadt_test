import React from "react";
import { BaseObject } from ".";

export interface Design extends BaseObject {
  courses: number;
  id: number;
  name: string;
  status: string;
  updated: Date;
  user_id_last_update: number | React.ReactNode;
  wales: number;
}
export type DesignList = Design[];
