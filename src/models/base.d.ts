import React from "react";

export interface BaseObject {
  [key: string]: number | string | Date | React.ReactNode;
}
export interface BaseStringObject {
  [key: string]: string;
}
