import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import styles from "./assets/css/app.module.css";
import { Navigation } from "./components";
import { Designs, Setouts } from "./containers";

const navigation = [
  { name: "Designs", path: "/designs" },
  { name: "Setouts", path: "/setouts" },
];
function App() {
  return (
    <div className={styles.container}>
      <Router>
        <Navigation options={navigation} />
        <div className={styles.appContainer}>
          <Switch>
            <Route exact path="/">
              <Redirect to="/designs" />
              <Designs />
            </Route>
            <Route path="/designs">
              <Designs />
            </Route>
            <Route path="/setouts">
              <Setouts />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
