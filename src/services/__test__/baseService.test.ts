import BaseService from "../baseService";

jest.spyOn(window, "fetch");

describe("Base service", () => {
  const service = new BaseService("fakeurl");
  const body = { body: "body" };
  const headers = {
    "Content-Type": "application/json",
  };

  it("GET method response OK", async () => {
    window.fetch.mockResolvedValueOnce({
      ok: true,
      json: async () => ({ success: true }),
    });
    // @ts-ignore
    await service.get();
    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith("fakeurl");
  });
  it("GET method response NOT OK", async () => {
    window.fetch.mockResolvedValueOnce({
      ok: false,
    });
    expect.assertions(1);
    // @ts-ignore
    return service
      .get()
      .catch((e) => expect(e.message).toEqual("Error while getting data"));
  });
  it("GET method response error", async () => {
    window.fetch.mockRejectedValueOnce({
      error: "no connection to the host",
    });
    expect.assertions(1);
    // @ts-ignore
    return service.get().catch((e) =>
      expect(e).toEqual({
        error: "no connection to the host",
      })
    );
  });

  it("PUT method response OK", async () => {
    window.fetch.mockResolvedValueOnce({
      ok: true,
      json: async () => ({ success: true }),
    });
    // @ts-ignore
    await service.put(52014, body);
    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith("fakeurl/52014", {
      method: "PUT",
      body: JSON.stringify(body),
      headers: headers,
    });
  });
  it("PUT method response NOT OK", async () => {
    window.fetch.mockResolvedValueOnce({
      ok: false,
    });
    expect.assertions(1);
    // @ts-ignore
    return service
      .put(52014, body)
      .catch((e) => expect(e.message).toEqual("Error while getting data"));
  });
  it("PUT method response error", async () => {
    window.fetch.mockRejectedValueOnce({
      error: "no connection to the host",
    });
    expect.assertions(1);
    // @ts-ignore
    return service.put(52014).catch((e) =>
      expect(e).toEqual({
        error: "no connection to the host",
      })
    );
  });
});
