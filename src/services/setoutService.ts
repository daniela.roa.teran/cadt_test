import { SetoutList, Setout } from "../models";
import BaseService from "./baseService";

export default class SetoutService extends BaseService {
  constructor() {
    super("http://localhost:5000/setouts");
  }
  async getAvailableSetouts() {
    let setouts: SetoutList = [];
    await this.get<SetoutList>()
      .then((data) => {
        setouts = data;
      })
      .catch((error) => console.log(error));
    return setouts;
  }

  async updateSetout(id: number | string, body: Setout) {
    let setout: Setout = {} as Setout;
    await this.put<Setout>(id, body)
      .then((data) => {
        setout = data;
      })
      .catch((error) => console.log(error));
    return setout;
  }
}
