import { DesignList, Design } from "../models";
import BaseService from "./baseService";

export default class DesingService extends BaseService {
  constructor() {
    super("http://localhost:5000/designs");
  }
  async getAvailableDesigns() {
    let designs: DesignList = [];
    await this.get<DesignList>()
      .then((data) => {
        designs = data;
      })
      .catch((error) => console.log(error));

    return designs;
  }
  async updateDesign(id: number | string, body: Design) {
    let design: Design = {} as Design;
    await this.put<Design>(id, body)
      .then((data) => {
        design = data;
      })
      .catch((error) => console.log(error));

    return design;
  }
}
