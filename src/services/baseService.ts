class BaseService {
  public url: string;

  constructor(url: string) {
    this.url = url;
  }

  protected async get<T>(): Promise<T> {
    return new Promise<T>(async (resolve, reject) => {
      await fetch(this.url)
        .then((response) => {
          if (!response.ok) throw new Error("Error while getting data");
          return response.json();
        })
        .then((data: T) => {
          resolve(data);
        })
        .catch((error: Error) => {
          reject(error);
        });
    });
  }

  protected async put<T>(id: number | string, body: T): Promise<T> {
    return new Promise<T>(async (resolve, reject) => {
      await fetch(`${this.url}/${id}`, {
        method: "PUT",
        body: JSON.stringify(body),
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((response) => {
          if (!response.ok) throw new Error("Error while getting data");
          return response.json();
        })
        .then((data: T) => {
          resolve(data);
        })
        .catch((error: Error) => {
          reject(error);
        });
    });
  }
}
export default BaseService;
