import BaseService from "./baseService";
import { UserList } from "../models";

export default class UserService extends BaseService {
  constructor() {
    super("http://localhost:5000/users");
  }
  async getUsers() {
    let users: UserList = [] as UserList;
    await this.get<UserList>()
      .then((data) => {
        users = data;
      })
      .catch((error) => console.log(error));
    return users;
  }
}
