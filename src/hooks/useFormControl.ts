import { useState, useEffect } from "react";
import { BaseObject, BaseStringObject } from "../models/base";

const useFormControl = (initialData: BaseObject) => {
  const [values, setValues] = useState({});
  const [errors, setErrors] = useState<BaseStringObject>({});

  useEffect(() => {
    setValues(initialData);
  }, [initialData]);

  const validate = (fields = values) => {
    let errors_aux = { ...errors };
    Object.entries(fields).map(([key, value]) => {
      return key in fields
        ? (errors_aux[key] = value ? "" : "this field is required")
        : null;
    });
    setErrors({ ...errors_aux });
  };

  const handleFieldChange = (
    e: React.ChangeEvent<HTMLInputElement> | React.ChangeEvent<any>
  ) => {
    const { id, value } = e.currentTarget;
    setValues({
      ...values,
      [id]: value,
    });
    validate({ [id]: value });
  };

  const isFormValid = (fields = values) => {
    const isValid =
      Object.keys(fields).every((f) => f in values) &&
      Object.values(errors).every((e) => e === "");
    return isValid;
  };

  const getEditableFields = (
    fields: BaseObject,
    editableFields: BaseStringObject[]
  ) => {
    let editable: BaseObject = {} as BaseObject;

    editableFields.map((val) => {
      if (val.dataKey in fields) {
        editable[val.dataKey] = fields[val.dataKey];
        if (val.dataType === "number")
          editable[val.dataKey] = parseInt(fields[val.dataKey] as string);
      }
      return true;
    });

    return editable;
  };
  const handleFormSubmit = (
    e: React.MouseEvent<HTMLButtonElement>,
    handleUpdate: Function,
    editableFields: BaseStringObject[]
  ) => {
    e.preventDefault();
    if (isFormValid()) {
      let formValues = getEditableFields(values, editableFields);
      handleUpdate({ ...formValues, updated: new Date() });
    }
  };
  return {
    handleFieldChange,
    isFormValid,
    handleFormSubmit,
    errors,
  };
};
export default useFormControl;
