import useFormControl from "../useFormControl";
import "@testing-library/jest-dom";
import React, { useState, useEffect } from "react";
import { shallow } from "enzyme";
import { act } from "react-dom/test-utils";

// jest.mock("react", () => ({
//   useState: jest.fn((f) => f()),
// }));

describe("useFormControl custom hook", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });
  it("Should not have errors", () => {
    let mockUseStateValues = jest
      .spyOn(React, "useState")
      .mockReturnValueOnce([{ name: "Tom", lastName: "Jerry" }, jest.fn()]);
    let mockUseStateErrors = jest
      .spyOn(React, "useState")
      .mockReturnValueOnce([{}, jest.fn()]);
    jest.spyOn(React, "useEffect").mockImplementation(jest.fn());
    let { isFormValid } = useFormControl({ name: "Tom", lastName: "Jerry" });

    expect(isFormValid()).toEqual(true);
    expect(mockUseStateValues).toHaveBeenCalled();
    expect(mockUseStateErrors).toHaveBeenCalled();
  });

  it("Should have erros when at least one field is empty", () => {
    jest.spyOn(React, "useState").mockReturnValueOnce([{}, jest.fn()]);
    jest
      .spyOn(React, "useState")
      .mockReturnValueOnce([{ name: "this field is required" }, jest.fn()]);
    jest.spyOn(React, "useEffect").mockImplementation(jest.fn());

    let { isFormValid } = useFormControl({ name: "", lastName: "Jerry" });
    expect(isFormValid()).toEqual(false);
  });

  it("change the value on handleChange function and return no errors", () => {
    jest
      .spyOn(React, "useState")
      .mockReturnValueOnce([{ name: "Jhon", lastName: "Jerry" }, jest.fn()]);
    jest.spyOn(React, "useState").mockReturnValueOnce([{}, jest.fn()]);
    jest.spyOn(React, "useEffect").mockImplementation(jest.fn());
    let { handleFieldChange, errors } = useFormControl({
      name: "",
      lastName: "Jerry",
    });

    handleFieldChange({ currentTarget: { id: "name", value: "Jhon" } });
    expect(errors).toEqual({});
  });

  it("change the value on handleChange function and return error", () => {
    jest
      .spyOn(React, "useState")
      .mockReturnValueOnce([{ name: "Jhon", lastName: "Jerry" }, jest.fn()]);

    jest
      .spyOn(React, "useState")
      .mockReturnValueOnce([{ name: "this field is required" }, jest.fn()]);
    jest.spyOn(React, "useEffect").mockImplementation(jest.fn());
    let { handleFieldChange, errors } = useFormControl({
      name: "",
      lastName: "Jerry",
    });
    handleFieldChange({ currentTarget: { id: "name", value: "" } });
    expect(errors).toEqual({ name: "this field is required" });
  });

  it("Form should have no errors on handleFormSubmit fuction", () => {
    jest
      .spyOn(React, "useState")
      .mockReturnValueOnce([{ name: "Jhon", lastName: "Jerry" }, jest.fn()]);

    jest.spyOn(React, "useState").mockReturnValueOnce([{}, jest.fn()]);
    jest.spyOn(React, "useEffect").mockImplementation(jest.fn());
    let { handleFormSubmit } = useFormControl({
      name: "Tom",
      lastName: "Jerry",
    });
    const mockPreventDefault = jest.fn();
    const mockHandleSubmit = jest.fn();
    const e = { preventDefault: mockPreventDefault };
    const editable = [
      {
        headerTitle: "Name",
        dataKey: "name",
        dataType: "string",
      },
      {
        headerTitle: "Last Name",
        dataKey: "lastName",
        dataType: "string",
      },
    ];
    handleFormSubmit(e, mockHandleSubmit, editable);
    expect(mockPreventDefault).toHaveBeenCalledTimes(1);
    expect(mockHandleSubmit).toHaveBeenCalledTimes(1);
  });
  it("Form should have no errors on handleFormSubmit fuction and dataType validation when it is number", () => {
    jest
      .spyOn(React, "useState")
      .mockReturnValueOnce([{ name: "Jhon", lastName: "Jerry" }, jest.fn()]);

    jest.spyOn(React, "useState").mockReturnValueOnce([{}, jest.fn()]);
    jest.spyOn(React, "useEffect").mockImplementation(jest.fn());
    let { handleFormSubmit } = useFormControl({
      name: "Tom",
      lastName: "Jerry",
    });
    const mockPreventDefault = jest.fn();
    const mockHandleSubmit = jest.fn();
    const e = { preventDefault: mockPreventDefault };
    const editable = [
      {
        headerTitle: "Name",
        dataKey: "name",
        dataType: "string",
      },
      {
        headerTitle: "Last Name",
        dataKey: "lastName",
        dataType: "number",
      },
    ];
    handleFormSubmit(e, mockHandleSubmit, editable);
    expect(mockPreventDefault).toHaveBeenCalledTimes(1);
    expect(mockHandleSubmit).toHaveBeenCalledTimes(1);
  });
  it("Form should not fire handleFormSubmit fuction when it has errors", () => {
    jest.spyOn(React, "useState").mockReturnValueOnce([{}, jest.fn()]);

    jest
      .spyOn(React, "useState")
      .mockReturnValueOnce([{ name: "this field is required" }, jest.fn()]);
    jest.spyOn(React, "useEffect").mockImplementation(jest.fn());
    let { handleFormSubmit } = useFormControl({
      name: "Tom",
      lastName: "Jerry",
    });
    const mockPreventDefault = jest.fn();
    const mockHandleSubmit = jest.fn();
    const e = { preventDefault: mockPreventDefault };
    handleFormSubmit(e, mockHandleSubmit, []);
    expect(mockPreventDefault).toHaveBeenCalledTimes(1);
    expect(mockHandleSubmit).not.toHaveBeenCalled();
  });
});
