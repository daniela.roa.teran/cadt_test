import { useEffect, useMemo, useState, useCallback } from "react";
import { SetoutService } from "../services";
import { SetoutList, ModalObject, Setout } from "../models";
import { List, EditModal } from "../components";

const Setouts = () => {
  const [data, setData] = useState<SetoutList>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [editModal, setEditModal] = useState<ModalObject>({
    open: false,
    data: {},
  });
  const setoutService = useMemo(() => new SetoutService(), []);

  const dataTable = [
    {
      headerTitle: "Name",
      dataKey: "name",
      dataType: "string",
    },
    {
      headerTitle: "Machine Name",
      dataKey: "machine_name",
      dataType: "string",
    },
    {
      headerTitle: "Machine Width",
      dataKey: "machine_width",
      dataType: "number",
    },
    {
      headerTitle: "Courses",
      dataKey: "courses",
      dataType: "number",
    },
    {
      headerTitle: "Last Updated",
      dataKey: "updated",
      dataType: "date",
    },
  ];
  const editableFields = [...dataTable];
  const handleEditModal = (event: React.MouseEvent<HTMLTableRowElement>) => {
    const { index } = event.currentTarget.dataset;
    let currentData;
    if (index) currentData = data[parseInt(index)];
    setEditModal({ open: true, data: currentData });
  };
  const getSeouts = useCallback(() => {
    setoutService.getAvailableSetouts().then((setouts) => {
      setData(setouts);
      setLoading(false);
    });
  }, [setoutService]);

  const handleUpdateData = (setout: Setout) => {
    setoutService.updateSetout(editModal.data.id, setout).then(() => {
      setEditModal({ open: false, data: {} });
      getSeouts();
    });
  };

  useEffect(() => {
    getSeouts();
  }, [getSeouts]);

  return (
    <>
      <EditModal
        open={editModal.open}
        data={editModal.data}
        title="Editing setout"
        editableFields={editableFields.splice(0, 4)}
        handleUpdate={handleUpdateData}
        handleClose={() => setEditModal({ ...editModal, open: false })}
      />
      <List
        dataTable={dataTable}
        data={data}
        title="setouts"
        loading={loading}
        onRowClick={handleEditModal}
      />
    </>
  );
};
export default Setouts;
