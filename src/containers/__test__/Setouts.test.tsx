import { Setouts } from "..";
import { shallow } from "enzyme";
import { act } from "@testing-library/react";
import { SetoutService } from "../../services";
import { EditModal, List } from "../../components";

const mockUpdateSetout = jest.fn().mockResolvedValue(true);
jest.mock("../../services/setoutService", () => {
  return jest.fn().mockImplementation(() => {
    return { updateSetout: mockUpdateSetout };
  });
});

describe("Setouts container", () => {
  const wrapper = shallow(<Setouts />);

  const setout = {
    name: "1st Setout",
    machine_name: "asd",
    machine_width: 250,
    courses: 1200,
    updated: "2021-07-19T16:49:59.838Z",
    id: 1,
  };
  it("Renders Modal component with its props", () => {
    expect(wrapper.find(EditModal)).toBeDefined();
    expect(wrapper.find(EditModal).props()).toBeDefined();
    expect(wrapper.find(EditModal).prop("open")).toEqual(false);
  });
  it("Renders List component with its props", () => {
    expect(wrapper.find(List)).toBeDefined();
    expect(wrapper.find(List).props()).toBeDefined();
  });
  it("Call handleEditModal on click List arrow", () => {
    expect(wrapper.find(List).prop("onRowClick")).toBeDefined();
    const e = { currentTarget: { dataset: "0" } };
    act(() => {
      wrapper.find(List).prop("onRowClick")(e);
    });
    wrapper.update();
    expect(wrapper.find(EditModal).prop("open")).toEqual(true);
  });

  it.skip("Call hadleUpdate data on click EditModal save", () => {
    expect(wrapper.find(EditModal).prop("handleUpdate")).toBeDefined();
    act(() => {
      wrapper.find(EditModal).prop("handleUpdate")(setout);
    });
    //SetoutService is mocked but when instance is called return undefined
    expect(mockUpdateSetout).toHaveBeenCalledTimes(1);
  });
});
