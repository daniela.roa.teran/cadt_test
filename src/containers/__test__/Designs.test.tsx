import { Designs } from "..";
import { shallow } from "enzyme";
import { act } from "@testing-library/react";
import { DesignService } from "../../services";
import { EditModal, List } from "../../components";

const mockUpdateDesign = jest.fn().mockResolvedValue(true);
jest.mock("../../services/designService", () => {
  return jest.fn().mockImplementation(() => {
    return { updateDesign: mockUpdateDesign };
  });
});

describe("Design container", () => {
  const wrapper = shallow(<Designs />);

  const design = {
    courses: 111,
    id: 20,
    name: "20th Design",
    status: "in-progress",
    updated: "2021-04-12 08:25:41.567611",
    user_id_last_update: 1,
    wales: 333,
  };
  it("Renders Modal component with its props", () => {
    expect(wrapper.find(EditModal)).toBeDefined();
    expect(wrapper.find(EditModal).props()).toBeDefined();
    expect(wrapper.find(EditModal).prop("open")).toEqual(false);
  });
  it("Renders List component with its props", () => {
    expect(wrapper.find(List)).toBeDefined();
    expect(wrapper.find(List).props()).toBeDefined();
  });
  it("Call handleEditModal on click List arrow", () => {
    expect(wrapper.find(List).prop("onRowClick")).toBeDefined();
    const e = { currentTarget: { dataset: "0" } };
    act(() => {
      wrapper.find(List).prop("onRowClick")(e);
    });
    wrapper.update();
    expect(wrapper.find(EditModal).prop("open")).toEqual(true);
  });

  it.skip("Call hadleUpdate data on click EditModal save", () => {
    expect(wrapper.find(EditModal).prop("handleUpdate")).toBeDefined();
    act(() => {
      wrapper.find(EditModal).prop("handleUpdate")(design);
    });
    //DesingService is mocked but when instance is called return undefined
    expect(mockUpdateDesign).toHaveBeenCalledTimes(1);
  });
});
