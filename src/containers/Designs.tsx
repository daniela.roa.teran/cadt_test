import { useEffect, useMemo, useState, useCallback } from "react";
import { DesignService, UserService } from "../services";
import { DesignList, ModalObject, Design } from "../models";
import { EditModal, List } from "../components";
import styles from "../assets/css/designs.module.css";

import { Avatar } from "@material-ui/core";
import Tooltip from "@material-ui/core/Tooltip";

var _ = require("lodash");

const Designs = () => {
  const [data, setData] = useState<DesignList>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [editModal, setEditModal] = useState<ModalObject>({
    open: false,
    data: {},
  });
  const designService = useMemo(() => new DesignService(), []);
  const userService = useMemo(() => new UserService(), []);
  const dataTable = [
    {
      headerTitle: "Name",
      dataKey: "name",
      dataType: "string",
    },
    {
      headerTitle: "Courses",
      dataKey: "courses",
      dataType: "number",
    },
    {
      headerTitle: "Wales",
      dataKey: "wales",
      dataType: "number",
    },
    {
      headerTitle: "Last Updated",
      dataKey: "updated",
      dataType: "date",
    },
    {
      headerTitle: "By",
      dataKey: "user_avatar",
      dataType: "string",
    },
  ];
  const editableFields = [...dataTable];

  const handleEditModal = (event: React.MouseEvent<HTMLTableRowElement>) => {
    const { index } = event.currentTarget.dataset;
    let currentData: Design = {} as Design;
    if (index) currentData = data[parseInt(index)];
    setEditModal({ open: true, data: currentData });
  };

  const handleUpdateData = (design: Design) => {
    designService.updateDesign(editModal.data.id, design).then(() => {
      setEditModal({ open: false, data: {} });
      getDesigns();
    });
  };

  const userAvatar = (name: string | undefined): React.ReactNode => {
    let initials = "-";
    if (name) {
      const fullName = name.split(" ");
      initials = fullName[0][0];
      if (fullName.length > 1) initials += fullName[1][0];
    }
    return (
      <Tooltip title={name ? name : initials} aria-label={initials}>
        <Avatar className={styles.avatar}>{initials}</Avatar>
      </Tooltip>
    );
  };

  const setUserAvatar = useCallback(
    (designs: DesignList) => {
      userService.getUsers().then((users) => {
        const usersAux = _.keyBy(users, "id");
        const designsAux: DesignList = [...designs];

        designsAux.map((d) => {
          let nameAux = undefined;
          if (typeof d.user_id_last_update === "number") {
            nameAux = usersAux[d.user_id_last_update as number].name;
          }
          return _.assign(d, {
            user_avatar: userAvatar(nameAux),
          });
        });
        setLoading(false);
        setData(designsAux);
      });
    },
    [userService]
  );
  const getDesigns = useCallback(() => {
    designService.getAvailableDesigns().then((designs) => {
      setUserAvatar(designs);
    });
  }, [designService, setUserAvatar]);

  useEffect(() => {
    getDesigns();
  }, [getDesigns]);

  return (
    <>
      <EditModal
        open={editModal.open}
        data={editModal.data}
        title="Editing design"
        editableFields={editableFields.splice(0, 3)}
        handleUpdate={handleUpdateData}
        handleClose={() => setEditModal({ ...editModal, open: false })}
      />
      <List
        dataTable={dataTable}
        data={data}
        title="designs"
        loading={loading}
        onRowClick={handleEditModal}
      />
    </>
  );
};
export default Designs;
