import React from "react";
import { BaseObject } from "../models";
import styles from "../assets/css/list.module.css";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import CircularProgress from "@material-ui/core/CircularProgress";
interface DataTable {
  headerTitle: string;
  dataKey: string;
}
// eslint-disable-next-line
interface ListProps<T = BaseObject> {
  dataTable: DataTable[];
  data: Array<any>;
  title: string;
  loading: boolean;
  onRowClick?: (e: React.MouseEvent<HTMLTableRowElement>) => void;
}

const List = <T,>({
  dataTable,
  data,
  title,
  loading,
  onRowClick,
}: ListProps<T>) => {
  const dataFormat = (dateString: string | number | React.ReactNode) => {
    if (typeof dateString === "string") {
      if (!isNaN(Date.parse(dateString))) {
        const date = new Date(dateString);
        dateString = `${date.getDate()}/${date.getMonth() + 1}/${date
          .getFullYear()
          .toString()
          .substring(2, 4)}`;
      }
    }
    return dateString;
  };

  return (
    <div className={styles.tableContainer}>
      <TableContainer component={Paper} className={styles.table}>
        {loading ? (
          <CircularProgress className={styles.loading} />
        ) : (
          <Table stickyHeader>
            <TableHead>
              <TableRow>
                {dataTable.map((op, i) => (
                  <TableCell
                    key={`header-cell-${op.headerTitle}-${i}`}
                    style={{ fontWeight: "bold" }}
                  >
                    {op.headerTitle}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row, i) => (
                <TableRow
                  key={`row-${i}-${title}`}
                  data-index={i}
                  onClick={onRowClick && onRowClick}
                  style={{ cursor: "pointer" }}
                >
                  {dataTable.map((key, f) => {
                    if (key.dataKey in row)
                      return (
                        <TableCell key={`body-cell-${i}-${f}`}>
                          {dataFormat(row[key.dataKey])}
                        </TableCell>
                      );
                    return null;
                  })}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        )}
      </TableContainer>
    </div>
  );
};
export default List;
