import styles from "../assets/css/editModal.module.css";
import useFormControl from "../hooks/useFormControl";
import { BaseStringObject } from "../models";

import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";

interface EditModalProps<T> {
  open: boolean;
  data: BaseStringObject;
  title: string;
  handleClose: () => void;
  handleUpdate: (s: T) => void;
  editableFields: BaseStringObject[];
}

const EditModal = <T,>({
  open,
  data,
  handleClose,
  handleUpdate,
  title,
  editableFields,
}: EditModalProps<T>) => {
  const { handleFieldChange, handleFormSubmit, errors, isFormValid } =
    useFormControl(data);

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
      className={styles.container}
    >
      <DialogTitle id="form-dialog-title">{title}</DialogTitle>
      <DialogContent>
        {editableFields.map((key, i) => {
          if (data[key.dataKey]) {
            let type = typeof data[key.dataKey] === "number" && "number";
            return (
              <TextField
                key={`editable-field-${key.dataKey}-${i}`}
                margin="normal"
                required
                fullWidth
                inputProps={{ type: type ? type : "text" }}
                id={key.dataKey}
                onBlur={handleFieldChange}
                onChange={handleFieldChange}
                label={key.headerTitle}
                defaultValue={data[key.dataKey]}
                {...(errors[key.dataKey] && {
                  error: true,
                  helperText: errors[key.dataKey],
                })}
              />
            );
          }
          return null;
        })}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} variant="outlined" color="primary">
          Cancel
        </Button>
        <Button
          color="primary"
          variant="contained"
          onClick={(e) => handleFormSubmit(e, handleUpdate, editableFields)}
          disabled={!isFormValid()}
        >
          Save changes
        </Button>
      </DialogActions>
    </Dialog>
  );
};
export default EditModal;
