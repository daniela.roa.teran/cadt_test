import { Navigation } from "../";
import { shallow } from "enzyme";

import { NavLink } from "react-router-dom";
import Drawer from "@material-ui/core/Drawer";

const navigation = [
  {
    name: "marvel",
    path: "/marvel",
  },
  {
    name: "dc",
    path: "/dc",
  },
  {
    name: "star wars",
    path: "/darkside",
  },
];
describe("Navigation component", () => {
  const wrapper = shallow(<Navigation options={navigation} />);
  it("onclick call function", () => {
    expect(
      wrapper.find(Drawer).find(NavLink).at(0).prop("onClick")
    ).toBeDefined();
    wrapper.find(Drawer).find(NavLink).at(0).prop("onClick")();
    expect(wrapper.find(Drawer).at(0).prop("open")).toEqual(true);
  });
  it("renders navigation options", () => {
    expect(wrapper.find(Drawer).at(0).find("ul").children().length).toEqual(
      navigation.length
    );
  });
});
