import { shallow } from "enzyme";
import { EditModal } from "..";
import useFormControl from "../../hooks/useFormControl";

import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";

jest.mock("../../hooks/useFormControl", () =>
  jest.fn().mockImplementation(() => ({
    handleFieldChange: jest.fn(),
    handleFormSubmit: jest.fn(),
    errors: {},
    isFormValid: jest.fn(),
  }))
);

const open = true;
const data = {
  courses: "111",
  id: "20",
  name: "20th Design",
  status: "in-progress",
  updated: "2021-04-12 08:25:41.567611",
  user_id_last_update: "1",
  wales: "333",
};
const handleClose = jest.fn();
const handleUpdate = jest.fn();
const title = "Amazing modal";
const editableFields = [
  {
    headerTitle: "Name",
    dataKey: "name",
    dataType: "string",
  },
  {
    headerTitle: "Courses",
    dataKey: "courses",
    dataType: "number",
  },
];

const defaultProps = {
  open,
  data,
  handleClose,
  handleUpdate,
  title,
  editableFields,
};
describe("Modal component", () => {
  const wrapper = shallow(<EditModal {...defaultProps} />);
  it("Renders Dialog with all its properties", () => {
    expect(wrapper.find(Dialog).prop("open")).toEqual(open);
    expect(wrapper.find(Dialog).prop("onClose")).toEqual(handleClose);
  });
  it("Renders DialogTitle text", () => {
    expect(wrapper.find(DialogTitle).text()).toEqual(title);
  });
  it("Renders all fields in DialogContent", () => {
    expect(wrapper.find(DialogContent).find(TextField).length).toEqual(
      editableFields.length
    );
  });
  it("Renders DialogActions buttons with its properties", () => {
    expect(
      wrapper.find(DialogActions).find(Button).at(0).prop("onClick")
    ).toEqual(handleClose);
    wrapper.find(DialogActions).find(Button).at(0).prop("onClick")();
    expect(handleClose).toHaveBeenCalledTimes(1);
    expect(
      wrapper.find(DialogActions).find(Button).at(1).prop("onClick")
    ).toBeDefined();
    wrapper.find(DialogActions).find(Button).at(1).prop("onClick")();
    //expect(handleFormSubmitMock).toHaveBeenCalledTimes(1);
  });
});
