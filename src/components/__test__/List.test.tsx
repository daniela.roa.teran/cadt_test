import { shallow } from "enzyme";
import { List } from "../";

import TableHead from "@material-ui/core/TableHead";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";

const dataTable = [
  {
    headerTitle: "Name",
    dataKey: "name",
  },
  {
    headerTitle: "Courses",
    dataKey: "courses",
  },
  {
    headerTitle: "Wales",
    dataKey: "wales",
  },
  {
    headerTitle: "Last Updated",
    dataKey: "updated",
  },
  {
    headerTitle: "By",
    dataKey: "user_id_last_update",
  },
];

const data = [
  {
    courses: 111,
    id: 20,
    name: "20th Design",
    status: "in-progress",
    updated: "2021-04-12 08:25:41.567611",
    user_id_last_update: 1,
    wales: 333,
  },
];
const title = "Designs";

const defaultProps = {
  dataTable: dataTable,
  data: data,
  title: title,
  loading: false,
};

describe("List component", () => {
  const wrapper = shallow(<List {...defaultProps} />);
  it("Renders header columns elements", () => {
    expect(wrapper.find(TableHead)).toBeDefined();
    expect(wrapper.find(TableHead).find(TableCell)).toHaveLength(
      dataTable.length
    );
  });
  it("Renders body rows elements", () => {
    expect(wrapper.find(TableBody)).toBeDefined();
    expect(wrapper.find(TableBody).find(TableRow)).toHaveLength(data.length);
  });
  it("Date field has correct format", () => {
    expect(wrapper.find(TableBody).text().includes("12/4/21")).toBe(true);
  });
  it("Does not render non-existent element", () => {
    const header = [
      {
        headerTitle: "By",
        dataKey: "user_id_last_update",
      },
      {
        headerTitle: "Cat",
        dataKey: "cat",
      },
    ];
    const newWrapper = shallow(<List {...defaultProps} dataTable={header} />);
    expect(newWrapper.find(TableBody).find(TableRow)).toHaveLength(1);
  });
});
